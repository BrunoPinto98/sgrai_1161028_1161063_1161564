//selection tool
var hoveredParts = [];
var selectedParts = [];

var sceneElementsXOffset = -10;
var sceneElementsYOffset = 0;
var sceneElementsZOffset = -1;

//the factor by which real life dimensions are scaled into the scene
var scalingFactor = 0.5;

//establish dynamic resizing of the scene dimensions
window.addEventListener('resize', onWindowResize, false);

var sceneHeight = window.innerHeight;
var sceneWidth = window.innerWidth;

let scene = new THREE.Scene();
//args : fov, aspect ratio, near, far
//aspect ratio should be in accordance with the renderer size!

let cameraTop = new THREE.PerspectiveCamera(75, sceneWidth / sceneHeight, 0.1, 1000);
cameraTop.position.y = 100;
cameraTop.position.z = 80; //se estiver z=80 e a seguir z, aparece, mas em vez de ser do topo roda de lado
cameraTop.rotation.z = Math.PI / 2;
let cameraDefault = new THREE.PerspectiveCamera(75, sceneWidth / sceneHeight, 0.1, 1000);
cameraDefault.position.z = 80;

var cameraList = [cameraTop, cameraDefault];

var camera = cameraDefault;

let renderer = new THREE.WebGLRenderer({
	canvas: document.getElementById('closet-view'),
	antialias: true
});

renderer.setClearColor(0xeeeeee);

var pixelRatio = 1;
renderer.setPixelRatio(pixelRatio);
renderer.setSize(sceneWidth * pixelRatio, sceneHeight * pixelRatio);

//GLTF external model import settings
renderer.gammaOutput = true;
renderer.gammaFactor = 2.2;

//Define raycaster to detect intersections with the mouse
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var composer, effectFXAA, onHoverOutlinePass, selectionOutlinePass;
var group = new THREE.Group();

//Scene configurations

//Lights
let thereBeAmbient = new THREE.AmbientLight(0xffffff, 0.5);
scene.add(thereBeAmbient);

let thereBeLight = new THREE.PointLight(0xffffff, 0.5);
thereBeLight.position.set(15, 20, 10);
scene.add(thereBeLight);

let pointLight2 = new THREE.PointLight(0xffffff, 0.5);
pointLight2.position.set(-15, -20, -10);
scene.add(pointLight2);

var widthCloset = scale(60);
var heightCloset = scale(120);
var depthCloset = scale(30);
var thicknessCloset = scale(3);

var closet = createSampleCloset();
var initialCloset = closet;

scene.add(group);

//Controls
var orbitControls = new THREE.OrbitControls(camera, renderer.domElement);
var transformControls = new THREE.TransformControls(camera, renderer.domElement);
transformControls.setMode("scale");

scene.add(transformControls);

// postprocessing
composer = new THREE.EffectComposer(renderer);
var renderPass = new THREE.RenderPass(scene, camera);
composer.addPass(renderPass);
composer.setSize(window.innerWidth * pixelRatio, window.innerHeight * pixelRatio);

onHoverOutlinePass = new THREE.OutlinePass(new THREE.Vector2(window.innerWidth, window.innerHeight), scene, camera);
onHoverOutlinePass.visibleEdgeColor.set('#ffffff');
onHoverOutlinePass.hiddenEdgeColor.set('#ffffff');
onHoverOutlinePass.edgeStrength = Number(5.2);
onHoverOutlinePass.edgeGlow = Number(0);
onHoverOutlinePass.edgeThickness = Number(1.8);
onHoverOutlinePass.pulsePeriod = Number(3);


selectionOutlinePass = new THREE.OutlinePass(new THREE.Vector2(window.innerWidth, window.innerHeight), scene, camera);
selectionOutlinePass.visibleEdgeColor.set('#005769');
selectionOutlinePass.hiddenEdgeColor.set('#005769');
selectionOutlinePass.edgeStrength = Number(5.2);
selectionOutlinePass.edgeGlow = Number(0);
selectionOutlinePass.edgeThickness = Number(1.8);
selectionOutlinePass.pulsePeriod = Number(3);

composer.addPass(onHoverOutlinePass);
composer.addPass(selectionOutlinePass);

effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
effectFXAA.uniforms['resolution'].value.set(1 / window.innerWidth, 1 / window.innerHeight);
effectFXAA.renderToScreen = true;

composer.addPass(effectFXAA);

function animate() {
	requestAnimationFrame(animate);
	orbitControls.update();
	composer.render();
	// render();
}

animate();

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth * pixelRatio, window.innerHeight * pixelRatio);
	composer.setSize(window.innerWidth * pixelRatio, window.innerHeight * pixelRatio);
	effectFXAA.uniforms['resolution'].value.set(1 / window.innerWidth, 1 / window.innerHeight);
	render();
}

function render() {
	renderer.render(scene, camera);
}

function scale(val) {
	return val * scalingFactor;
}

function createClosetGeometry(width, height, depth, thickness) {

	let closetGeometry = new THREE.Geometry();

	//Create faces of the closet

	//position of faces of closet are defined relatively to the position of the backface
	var backfaceX = 0;
	var backfaceY = 0;
	var backfaceZ = 0;

	let backFace = new THREE.BoxGeometry(width, height, thickness);
	let backFaceMesh = new THREE.Mesh(backFace);

	backFaceMesh.position.set(backfaceX, backfaceY, backfaceZ);

	let rightSideFace = new THREE.BoxGeometry(thickness, height, depth);
	let rightSideFaceMesh = new THREE.Mesh(rightSideFace);

	rightSideFaceMesh.position.set(backfaceX + width * 0.5 - thickness * 0.5, backfaceY, backfaceZ + depth * 0.5);

	let leftSideFace = new THREE.BoxGeometry(thickness, height, depth);
	let leftSideFaceMesh = new THREE.Mesh(leftSideFace);

	leftSideFaceMesh.position.set(backfaceX - width * 0.5 + thickness * 0.5, backfaceY, backfaceZ + depth * 0.5);

	let topFace = new THREE.BoxGeometry(width, thickness, depth);
	let topFaceMesh = new THREE.Mesh(topFace);

	topFaceMesh.position.set(backfaceX, backfaceY + height * 0.5 - thickness * 0.5, backfaceZ + depth * 0.5);

	let bottomFace = new THREE.BoxGeometry(width, thickness, depth);
	let bottomFaceMesh = new THREE.Mesh(bottomFace);

	bottomFaceMesh.position.set(backfaceX, backfaceY - height * 0.5 + thickness * 0.5, backfaceZ + depth * 0.5);

	//since each mesh has had its position changed
	//updating matrices is required before merging the meshes
	//in order to mantain the established positions
	backFaceMesh.updateMatrix();
	rightSideFaceMesh.updateMatrix();
	leftSideFaceMesh.updateMatrix();
	topFaceMesh.updateMatrix();
	bottomFaceMesh.updateMatrix();

	//Merge faces into closet
	closetGeometry.merge(backFaceMesh.geometry, backFaceMesh.matrix);
	closetGeometry.merge(rightSideFaceMesh.geometry, rightSideFaceMesh.matrix);
	closetGeometry.merge(leftSideFaceMesh.geometry, leftSideFaceMesh.matrix);
	closetGeometry.merge(topFaceMesh.geometry, topFaceMesh.matrix);
	closetGeometry.merge(bottomFaceMesh.geometry, bottomFaceMesh.matrix);

	return closetGeometry;
}

function addSimpleShelves(initialHeight, finalHeight, numberOfShelves, width, height, depth, thickness, selectedContainer) {

	let simpleShelvesGeometry = new THREE.Geometry();

	let heightDifference = finalHeight - initialHeight;
	let shelfHeight = heightDifference / (numberOfShelves + 1);

	let shelf = new THREE.BoxGeometry(width - 2 * thickness, thickness, depth - thickness);

	var shelfPositionZ;
	if (initialHeight === 0) {
		if (finalHeight === height) {
			shelfPositionZ = -height / 2 + shelfHeight; //base + espaço de uma prateleira
		} else {
			// topo - espaço do nr de espaçamentos (prateleiras + 1)
			shelfPositionZ = (initialHeight + height / 2) - (shelfHeight * numberOfShelves) - thickness;
		}
	} else {
		shelfPositionZ = -height / 2 + shelfHeight; //base + espaço de uma prateleira
	}

	// var shelfPositionZ = (initialHeight + height/2) - shelfHeight; //(0 (centro) + metade da altura do armario (para começar no topo)) - altura inicial
	for (i = 0; i < numberOfShelves; i++) {
		let shelfMesh = new THREE.Mesh(shelf);
		// scene.add(shelfMesh);
		shelfMesh.position.set(0, shelfPositionZ, depth / 2);

		shelfMesh.updateMatrix();
		simpleShelvesGeometry.merge(shelfMesh.geometry, shelfMesh.matrix);

		shelfPositionZ += +shelfHeight;
	}

	let shelvesModuleMesh = new THREE.Mesh(
		simpleShelvesGeometry,
		new THREE.MeshLambertMaterial({
			map: new THREE.TextureLoader().load("../assets/textures/woodBase.png")
		})
	);

	shelvesModuleMesh.position.set(sceneElementsXOffset + widthCloset * selectedContainer, pageYOffset + 0, 0);

	return shelvesModuleMesh;

}

function addDrawersToCloset(initialHeight, finalHeight, numberOfDrawers, width, height, depth, thickness, selectedContainer) {

	let simpleDrawersGeometry = new THREE.Geometry();

	let heightDifference = finalHeight - initialHeight;
	let drawerHeight = heightDifference / (numberOfDrawers + 0.5);

	let drawer = new THREE.BoxGeometry(width - 2 * thickness, thickness, depth - thickness);
	let drawerFrontWall = new THREE.BoxGeometry(width - 2 * thickness, drawerHeight + thickness / 2, thickness);

	var drawerPositionZ;
	if (initialHeight === 0) {
		if (finalHeight === height) {
			drawerPositionZ = -height / 2 + thickness;
		} else {
			drawerPositionZ = (initialHeight + height / 2) - heightDifference - 2 * thickness;
		}
	} else {
		drawerPositionZ = -height / 2 + thickness;
	}

	for (i = 0; i < numberOfDrawers; i++) {
		let drawerMesh = new THREE.Mesh(drawer);
		drawerMesh.position.set(0, drawerPositionZ, depth / 2);

		let drawerFrontWallMesh = new THREE.Mesh(drawerFrontWall);
		drawerFrontWallMesh.position.set(0, drawerPositionZ + drawerHeight / 2, depth - 1);

		drawerMesh.updateMatrix();
		drawerFrontWallMesh.updateMatrix();

		simpleDrawersGeometry.merge(drawerMesh.geometry, drawerMesh.matrix);
		simpleDrawersGeometry.merge(drawerFrontWallMesh.geometry, drawerFrontWallMesh.matrix);

		drawerPositionZ -= -thickness - drawerHeight;
	}

	if (initialHeight != 0) {
		//upper level
		let drawerMesh = new THREE.Mesh(drawer);
		drawerMesh.position.set(0, drawerPositionZ - thickness, depth / 2);
		drawerMesh.updateMatrix();
		simpleDrawersGeometry.merge(drawerMesh.geometry, drawerMesh.matrix);
		simpleDrawersGeometry.merge(drawerMesh.geometry, drawerMesh.matrix);
	}

	let drawerModuleMesh = new THREE.Mesh(
		simpleDrawersGeometry,
		new THREE.MeshLambertMaterial({
			map: new THREE.TextureLoader().load("../assets/textures/woodBase.png")
		})
	);

	drawerModuleMesh.position.set(sceneElementsXOffset + widthCloset * selectedContainer, sceneElementsYOffset + 0, sceneElementsZOffset);

	return drawerModuleMesh;
}

function addDoor(width, height, thickness, selectedContainer) {

	let doorGeometry = new THREE.Geometry();

	let door = new THREE.BoxGeometry(width - 2 * thickness, height - 2 * thickness, thickness);
	let doorMesh = new THREE.Mesh(door);

	doorMesh.updateMatrix();
	doorGeometry.merge(doorMesh.geometry, doorMesh.matrix);

	let doorModuleMesh = new THREE.Mesh(
		doorGeometry,
		new THREE.MeshLambertMaterial({
			map: new THREE.TextureLoader().load("../assets/textures/woodBase.png")
		})
	);

	doorModuleMesh.position.set(sceneElementsXOffset + widthCloset * selectedContainer, sceneElementsYOffset + 0, depthCloset - thickness);

	return doorModuleMesh;
}

function addHanger(width, thickness, selectedContainer) {

	let cylinder = new THREE.CylinderGeometry(0.5, 0.5, width - 2 * thickness, 32);
	let hangerMesh = new THREE.Mesh(
		cylinder,
		new THREE.MeshLambertMaterial({
			map: new THREE.TextureLoader().load("../assets/textures/galvanizedMetal.jpg")
		})
	);

	hangerMesh.rotation.z = Math.PI / 2;

	hangerMesh.position.set(sceneElementsXOffset + widthCloset * selectedContainer, 25, 15 / 2);

	hangerMesh.updateMatrix();

	return hangerMesh;
}

function addClosetsToScene(orderOfCloset) {

	let closetsGeometry = new THREE.Geometry();

	let closet = createClosetGeometry(widthCloset, heightCloset, depthCloset, thicknessCloset);

	let closetMesh = new THREE.Mesh(
		closet,
		new THREE.MeshLambertMaterial({
			map: new THREE.TextureLoader().load("../assets/textures/woodBase.png")
		})
	);

	closetMesh.position.set(sceneElementsXOffset + widthCloset * orderOfCloset, sceneElementsYOffset + 0, sceneElementsZOffset + 0);

	closetMesh.updateMatrix();
	group.add(closetMesh);

	return closetsGeometry;
}

// function createSampleCloset() {
// 	//Add closetGeometry to scene
// 	let closets = new THREE.Mesh(
// 		addClosetsToScene(3),
// 		new THREE.MeshLambertMaterial({
// 			map: new THREE.TextureLoader().load("../assets/textures/woodBase.png")
// 		})
// 	);

// 	// closets.position.set(-10 + 0, 0, -1);

// 	//Add simple shelves to scene
// 	let simpleShelves = new THREE.Mesh(
// 		addSimpleShelves(0, heightCloset, depthCloset wthicknessClosethCloset, heightCloset, depthCloset, thicknessCloset),
// 		new THREE.MeshLambertMaterial({
// 			map: new THREE.TextureLoader().load("../assets/textures/woodBase.png")
// 		})
// 	);

// 	simpleShelves.position.set(-10 + widthCloset, 0, 0);

// 	//Add drawers to scene
// 	let drawers = new THREE.Mesh(
// 		addDrawersToCloset(widthCloset, heightCloset, depthCloset wthicknessClosethCloset, heightCloset, depthCloset, thicknessCloset),
// 		new THREE.MeshLambertMaterial({
// 			map: new THREE.TextureLoader().load("../assets/textures/woodBase.png")
// 		})
// 	);

// 	drawers.position.set(-10 + widthCloset * 2, 0, -1);

// 	//Add door to a closet
// 	let door = new THREE.Mesh(
// 		addDoor(widthCloset, heightCloset, depthCloset5)thicknessCloset/ 		new THREE.MeshLambertMaterial({
// 			map: new THREE.TextureLoader().load("../assets/textures/woodBase.png")
// 		})
// 	);

// 	door.position.set(-10 + widthCloset * 0, 0, 15 - 1.5);

// 	//Add hanger to a closet
// 	let hanger = new THREE.Mesh(
// 		addHanger(widthCloset, 1.5),
// 		new THREE.MeshLambertMaterial({
// 			color: 0x1B1010
// 		})
// 	);

// 	hanger.position.set(-10 + widthCloset * 2, 25, 15 / 2); //altura dada em relaçao ao centro 0

// 	closets.updateMatrix();
// 	simpleShelves.updateMatrix();
// 	drawers.updateMatrix();
// 	door.updateMatrix();
// 	hanger.updateMatrix();

// 	let sampleClosetMesh = new THREE.Mesh();

// 	group.add(closets);
// 	group.add(simpleShelves);
// 	group.add(drawers);
// 	group.add(door);
// 	group.add(hanger);

// 	return sampleClosetMesh;
// }

function createSampleCloset() {
	//Add closetGeometry to scene
	let closets = new THREE.Mesh(
		addClosetsToScene(0),
		new THREE.MeshLambertMaterial({
			map: new THREE.TextureLoader().load("../assets/textures/woodBase.png")
		})
	);

	let sampleClosetMesh = new THREE.Mesh();

	group.add(closets);

	return sampleClosetMesh;
}