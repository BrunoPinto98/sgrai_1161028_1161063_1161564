//Tools
function enableTransformTool() {
    orbitControls.enabled = false;
    transformControls.attach(group);
    disableSelectionTool();
}

function enableMoveTool() {
    orbitControls.enabled = true;
    transformControls.detach(group);
    disableSelectionTool();
}

function enableSelectionTool() {
    window.addEventListener('mousemove', onTouchMove);
    window.addEventListener('touchmove', onTouchMove);
    window.addEventListener('click', onClick);
}

function disableSelectionTool() {
    window.removeEventListener('mousemove', onTouchMove);
    window.removeEventListener('touchmove', onTouchMove);
    window.removeEventListener('click', onClick);
    onHoverOutlinePass.selectedObjects = [];
}

clearChanges = () => {
    alert('Are you sure you want to clear all of your changes and start over?');
}

var i = 0;

function changeCameraTool() { //changes camera in loop
    if (i >= 0 && i < cameraList.length - 1) {
        i++;
    } else {
        i = 0;
    }
    camera = cameraList[i];
}

//GUI
populateTextureContainer = () => {
    let texturePath = "assets/textures/"
    let textures = [
        texturePath + "woodBase.png",
        texturePath + "tri_pattern.jpg",
        texturePath + "image2.jpg",
        texturePath + "dirtyWoodPlanks.jpg",
        texturePath + "dirtyWoodPlanks2.jpg",
        texturePath + "dirtyWoodPlanks3.jpg",
        texturePath + "dirtyWoodPlanks4.jpg",
        texturePath + "cleanWoodPlanks.jpg",
        texturePath + "galvanizedMetal.jpg",

    ];

    let container = document.getElementById('textureDiv');

    for (var index = 0; index < textures.length; index++) {
        container.innerHTML += '<img src="' + textures[index] + '" ' +
            'class="textureItem"/>'
    }

    $('.textureItem').on('click', changeTexture);

}

var numberOfClosets = 1;

window.onload = () => {
    populateTextureContainer();
    updateContainingClosetSelector();
    updateForm();
}

//Event Handlers
function onTouchMove(event) {
    var x, y;
    if (event.changedTouches) {
        x = event.changedTouches[0].pageX;
        y = event.changedTouches[0].pageY;
    } else {
        x = event.clientX;
        y = event.clientY;
    }
    mouse.x = (x / window.innerWidth) * 2 - 1;
    mouse.y = -(y / window.innerHeight) * 2 + 1;
    checkIntersection();
}

function onClick(event) {
    var x, y;
    if (event.changedTouches) {
        x = event.changedTouches[0].pageX;
        y = event.changedTouches[0].pageY;
    } else {
        x = event.clientX;
        y = event.clientY;
    }
    mouse.x = (x / window.innerWidth) * 2 - 1;
    mouse.y = -(y / window.innerHeight) * 2 + 1;
    highlightIntersection();
}

changeTexture = (event) => {

    if (selectedParts.length) {
        for (var i = 0; i < selectedParts.length; i++) {
            let currentPart = selectedParts[i];
            let texturePath = event.currentTarget.src;
            currentPart.material.map = new THREE.TextureLoader().load(texturePath);
        }
    } else {
        alert("Please select a part first.");
    }
}

//Part selection
function addHoveredPart(object) {
    hoveredParts = [];
    hoveredParts.push(object);
}

function addSelectedPart(object) {
    selectedParts = [];
    selectedParts.push(object);
}

function checkIntersection() {
    raycaster.setFromCamera(mouse, camera);
    var intersects = raycaster.intersectObjects([scene], true);
    if (intersects.length > 0) {
        var hoveredObject = intersects[0].object;
        addHoveredPart(hoveredObject);
        onHoverOutlinePass.selectedObjects = hoveredParts;
    }
}

function highlightIntersection() {
    raycaster.setFromCamera(mouse, camera);
    var intersects = raycaster.intersectObjects([scene], true);
    if (intersects.length > 0) {
        var selectedObject = intersects[0].object;
        addSelectedPart(selectedObject);
        selectionOutlinePass.selectedObjects = selectedParts;
    }
}

//Forms

//Catalog
var closetSelector = document.getElementById('containingClosetSelector');

var Parts = {
    "hanger": {},
    "drawer": {},
    "shelf": {},
    "door": {},
    "separator": {}
};

//default value
var selectedPartToAdd = Parts.hanger;

triggerAddHanger = (event) => selectedPartToAdd = Parts.hanger;

triggerAddDrawers = (event) => selectedPartToAdd = Parts.drawer;

triggerAddShelves = (event) => selectedPartToAdd = Parts.shelf;

triggerAddDoor = (event) => selectedPartToAdd = Parts.door;

triggerAddSeparator = (event) => selectedPartToAdd = Parts.separator;

updateContainingClosetSelector = () => {
    closetSelector.innerHTML = "";
    for (var i = 0; i < numberOfClosets; i++) {
        closetSelector.innerHTML += '<option value="' + i + '">Separator ' + (i + 1) + '</option>';
    }
}

addPart = () => {

    switch (selectedPartToAdd) {
        case Parts.hanger:
            group.add(addHanger(widthCloset, thicknessCloset, closetSelector.value));
            break;
        case Parts.drawer:
            delegateAddDrawer();
            break;
        case Parts.shelf:
            delegateAddShelf();
            break;
        case Parts.door:
            group.add(addDoor(widthCloset, heightCloset, thicknessCloset, closetSelector.value));
            break;
        case Parts.separator:
            group.add(addClosetsToScene(numberOfClosets));
            numberOfClosets++;
            updateContainingClosetSelector();
            break;
        default:
            alert("Please select a part from the catalog");
    }

}

delegateAddDrawer = () => {
    let numDrawers = parseInt(document.getElementById('numberElementsInputField').value);
    let minHeight = parseInt(document.getElementById('minHeightInputField').value);
    let maxHeight = parseInt(document.getElementById('maxHeightInputField').value);

    if (numDrawers > 0 && minHeight >= 0 && maxHeight > 0) {

        group.add(
            addDrawersToCloset(minHeight, maxHeight, numDrawers, widthCloset, heightCloset, depthCloset, thicknessCloset, closetSelector.value)
        );

    } else alert("Please input positive integers in the fields");
}

delegateAddShelf = () => {
    let numShelves = parseInt(document.getElementById('numberElementsInputField').value);
    let minHeight = parseInt(document.getElementById('minHeightInputField').value);
    let maxHeight = parseInt(document.getElementById('maxHeightInputField').value);

    if (numShelves > 0 && minHeight >= 0 && maxHeight > 0) {

        group.add(
            addSimpleShelves(minHeight, maxHeight, numShelves, widthCloset, heightCloset, depthCloset, thicknessCloset, closetSelector.value)
        );

    } else alert("Please input positive integers in the fields");
}

removePart = () => {
    if (selectionOutlinePass.selectedObjects.length > 0) {
        group.remove(selectionOutlinePass.selectedObjects[0]);
    } else {
        alert("Please select the part you want to remove with the Select Tool.");
    }
}

//Global Dimensions

function updateForm() {
    let selectedObject = selectionOutlinePass.selectedObjects.length > 0 ?
        selectionOutlinePass.selectedObjects[0] :
        group;

    let hitbox = new THREE.Box3().setFromObject(selectedObject);
    document.getElementById("fHeight").value = hitbox.getSize().y.toFixed(2);
    document.getElementById("fWidth").value = hitbox.getSize().x.toFixed(2);
    document.getElementById("fDepth").value = hitbox.getSize().z.toFixed(2);
    setInterval(updateForm, 1);
}